=pod

=head1 SYNOPSIS

This tests the C<Example> class.

=cut


use strict;
use warnings;

use lib 't/lib';


use File::Spec;
use Test::Deep;
use Test::Exception;
use Test::More;


plan(tests => 6);

use_ok('Example');

my $config_name = 'config';
my $config_dir  = 't/';

my $app = Example->new({
    config_name => $config_name,
    config_path => [ $config_dir ],
});

my $config = $app->config;
isa_ok($config, 'HASH', 'Config method returns hash');

my $expected_config = {
    foo => 'bar',

    'Command::hello' => {
        name => 'Alice',
    },
};
cmp_deeply($config, $expected_config, 'Got expected config');

my $suffix = File::Spec->catfile($config_dir, $config_name);
my $expected_config_file = qr/$suffix/;
like($app->config_file, $expected_config_file, 'Got expected config_file name.');


subtest 'missing config file' => sub {
    plan(tests => 2);

    my $app2 = Example->new({
        config_path => [ '/yyyyyyyyyy', '/zzzzzzzzzzzzz' ],
    });

    my $config;
    lives_ok {
        $config = $app2->config;
    };

    my $expected_config = {};
    cmp_deeply($config, $expected_config, 'Got expected config');
};


subtest 'env test' => sub {
    plan(tests => 1);

    my $app2 = Example->new;

    my $expected_config_file = '/foo.conf';

    $app2->process_env_options({
        EXAMPLE_CONFIG_FILE => $expected_config_file,
    });

    is($app2->config_file, $expected_config_file, 'Got expected config_file');
};


