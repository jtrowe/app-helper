=pod

=head1 SYNOPSIS

This tests the command line interace of the C<Example> application.

=cut


use strict;
use warnings;

use lib 't/lib';

use App::Cmd::Tester;
use Data::Dump qw( dump );
use File::Spec;
use Test::Deep;
use Test::More;

use Example;


plan(tests => 5);

my @args;

@args = ( '' );
subtest 'args: ' . join(' ', @args) => sub {
    plan(tests => 1);

    my $app = Example->new;
    my $result = test_app($app => \@args);

    is($result->exit_code, 0, 'Good exit code') or do {
        note("stderr:\n" . $result->stderr);
        note("stdout:\n" . $result->stdout);
        note("error:\n" . $result->error);
        note("run_rv:\n" . $result->run_rv);
    };
};


@args = ( '--help' );
subtest 'args: ' . join(' ', @args) => sub {
    plan(tests => 1);

    my $app = Example->new;
    my $result = test_app($app => \@args);

    is($result->exit_code, 0, 'Good exit code') or do {
        note("stderr:\n" . $result->stderr);
        note("stdout:\n" . $result->stdout);
        note("error:\n" . $result->error);
        note("run_rv:\n" . $result->run_rv);
    };
};


my $config_name     = 'config';
my @config_path     = ( 't/', '/tmp/' );
my $config_path_str = join ':', @config_path;
@args = ( '--config-name', $config_name, '--config-path', $config_path_str );
subtest 'args: ' . join(' ', @args) => sub {
    plan(tests => 4);

    my $expected_config = {
        foo => 'bar',

        'Command::hello' => {
            name => 'Alice',
        },
    };

    my $app = Example->new;
    my $result = test_app($app => \@args);

    is($result->exit_code, 0, 'Good exit code') or do {
        note("stderr:\n" . $result->stderr);
        note("stdout:\n" . $result->stdout);
        note("error:\n" . $result->error);
        note("run_rv:\n" . $result->run_rv);
    };

    is($app->config_name, $config_name, 'Got correct config_name');
    cmp_deeply($app->config_path, \@config_path, 'Got correct config_path');

    cmp_deeply($app->config, $expected_config, 'Got expected config');

    note('app.config_file=' . $app->config_file);
};


$config_name     = 'config3';
@config_path     = ( '/tmp/' );
$config_path_str = join ':', @config_path;
@args = ( '--config-name', $config_name, '--config-path', $config_path_str );
subtest 'args: ' . join(' ', @args) => sub {
    plan(tests => 1);

    my $app = Example->new;
    my $result = test_app($app => \@args);

    is($result->exit_code, 0, 'Good exit code') or do {
        note("stderr:\n" . $result->stderr);
        note("stdout:\n" . $result->stdout);
        note("error:\n" . $result->error);
        note("run_rv:\n" . $result->run_rv);
    };

};


my $expected_config_name     = 'config2';
my $expected_config_dir      = 't/';
my $expected_config_path_str = join ':', @config_path;
my $config_file
        = File::Spec->catfile($expected_config_dir, $expected_config_name);
@args = ( '--config-file', $config_file );
subtest 'args: ' . join(' ', @args) => sub {
    plan(tests => 5);

    my $expected_config = {
        foo => 'baz',
    };

    my $app = Example->new;
    my $result = test_app($app => \@args);

    is($result->exit_code, 0, 'Good exit code') or do {
        note("stderr:\n" . $result->stderr);
        note("stdout:\n" . $result->stdout);
        note("error:\n" . $result->error);
        note("run_rv:\n" . $result->run_rv);
    };

    is_deeply($app->config, $expected_config, 'Got expected config');

    is($app->config_name, $expected_config_name, 'Got correct config_name');
    cmp_deeply($app->config_path, [ $expected_config_dir ],
            'Got correct config_path') or do {

        diag("app.config_name:\n" . $app->config_name);
        diag("app.config_path:\n" . dump($app->config_path));
    };

    my $file_re = qr/$config_file$/;
    like($app->config_file, $file_re, 'Got correct config_file');

};


