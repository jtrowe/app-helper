use strict;
use warnings;

use lib 't/lib';

use Test::Deep;
use Test::More;

plan(tests => 7);

use_ok('Example');
use_ok('Example::Command::hello');

my $config_name = 'config';
my $config_dir  = 't/';

my $app = Example->new({
    config_name => $config_name,
    config_path => [ $config_dir ],
});

my ( $cmd, $opt, $args ) = $app->prepare_command('hello');
isa_ok($cmd, 'Example::Command::hello');

is($cmd->description, $cmd->abstract,
        'Description is same as the abstract');


my $log = $cmd->log;
isa_ok($log, 'Log::Any::Proxy', 'Log method returns Log::Any class');

my $config = $cmd->config;
isa_ok($config, 'HASH', 'Config method returns hash');

my $expected_config = {
    name => 'Alice',
};
cmp_deeply($config, $expected_config, 'Got expected config');

