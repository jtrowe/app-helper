= Morlock

Some classes to help me write CLI apps quickly.


== Running Example Application

```sh
perl -Ilib -It/lib t/bin/example
```

== TODO

*   ::LoggingInitializer : log_level : make sure this is set at end
    of logging initialization.
    If initialized from a file, will need to figure it out and
    set it retroactively.
*   ::Base : process_global_options : falling back to env options
    for only the config options.  Not for others that have been
    added.
