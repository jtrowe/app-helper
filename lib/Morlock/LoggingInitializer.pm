package Morlock::LoggingInitializer;


# ABSTRACT: A role which provides logging initialization

# COPYRIGHT


use Moose::Role;

with qw( Morlock::ConfigurationLoader );
with qw( Morlock::Loggable );


use File::Basename;
use File::Spec;
use Log::Any::Adapter;
use Log::Log4perl;


=attr is_logging_initialized

Whether the logging system has been fully initialized.

=over

=item Type: Boolean

=item Default: false

=back

=cut

has is_logging_initialized => (
    is       => 'rw',
    isa      => 'Bool',
    default  => 0,
);


=attr logging_config_filename

The filename of the logging config file.

=over

=item Type: String

=item Optional: true

=back

=cut

has 'logging_config_filename' => (
    is       => 'rw',
    isa      => 'Str',
    required => 0,
);


=attr logging_config_file

The logging config file.

=over

=item Type: String

=item Optional: true

=back

=cut

has 'logging_config_file' => (
    is       => 'rw',
    isa      => 'Str',
    required => 0,
);


=attr quietness

The number of levels to decrease logging noisiness by.

=over

=item Type: Integer

=item Default: 0

=back

=cut

has 'quietness' => (
    is       => 'rw',
    isa      => 'Int',
    default  => 0,
);


=attr verbosity

The number of levels to increase logging noisiness by.

=over

=item Type: Integer

=item Default: 0

=back

=cut

has 'verbosity' => (
    is       => 'rw',
    isa      => 'Int',
    default  => 0,
);


=method adjust_verbosity

Using the C<quietness> and C<verbosity> attributes, adjusts
the noisiness of the root logging level.

Parameters:

=over

=item * C<level> Integer

The number of levels to adjust logging noisiness by.
Positive numbers will increase noisiness.
Negative numbers will decrease noisiness.

=back

=cut

sub adjust_verbosity {
    my $self       = shift;
    my $adjustment = shift;

    if ( 0 == $adjustment ) {
        return;
    }

    my $root = $self->root_logger;

    my $verb = ( $adjustment > 0 ) ? 'Increasing' : 'Decreasing';
    $self->log->info(sprintf '%s verbosity by %d levels.',
            $verb, abs($adjustment));

    if ( $adjustment > 0 ) {
        $root->more_logging($adjustment);
    }
    elsif ( $adjustment < 0 ) {
        $root->less_logging(-1 * $adjustment);
    }

} # adjust_verbosity


=method find_logging_config_file

Finds the location of the logging configuration file.

Uses the C<logging_config_file> attribute.

If it is an absolute path, then that exact file will be used.

If it is not an absolute path, then the file will be searched for
in the directory where the configuration file was located.

=cut

sub find_logging_config_file {
    my $self = shift;

    my $log_config = $self->logging_config_file;
    unless ( $log_config ) {
        return;
    }

    unless ( File::Spec->file_name_is_absolute($log_config) ) {
        # NB: self.config_file should be an absolute path at this point
        my $path = dirname($self->config_file);
        $log_config = File::Spec->catfile($path, $log_config);
    }

    return $log_config;
} # find_logging_config_file


=attr log_level

The log level of the root logger.

If this has been initialized from a logging configuration file,
it will not currently be set.

=over

=item Type: String

=item Default: Return value from C<default_log_level> method.

=back

=cut

has 'log_level' => (
    is       => 'rw',
    isa      => 'Str',
    builder  => 'default_log_level',
    lazy     => 1,
    required => 0,
);


=method default_log_level

Returns the default for the C<log_level> attribute.

Returns:

A string representation of a log level.
One of the following:

=over

=item * C<''>

=item * C<OFF>

=item * C<FATAL>

=item * C<ERROR>

=item * C<INFO>

=item * C<DEBUG>

=item * C<TRACE>

=item * C<ALL>

=back

=cut

sub default_log_level {
    return '';
} # default_log_level


=head2 initialize_basic_logging

Initializes the core of the logging system.

It will use the file specified as the
C<log_config_file>
attribute if it is defined.
Otherwise it will use the
C<easy_init>
initialization with the log level specified by the
C<log_level>
attribute.

=cut

sub initialize_basic_logging {
    my $self = shift;

    my $config_file = $self->find_logging_config_file;
    if ( $config_file ) {
        Log::Log4perl->init($config_file);
        $self->logging_config_file($config_file);
        $self->log->debug(sprintf 'Logging initialized from file %s',
                $config_file);
    }
    else {
        my $level = $self->log_level || 'info';
        Log::Log4perl->easy_init(
                $self->log_level_to_priority($level));
        $self->log->debug(sprintf 'Logging initialized using log-level %s',
                $level);
    }

} # initialize_basic_logging


=head2 initialize_bootstrap_logging

Initializes the logging system used at the very beginning of execution.

None of this is configurable at the moment because it happens before
any processing of configuration information takes place.

=cut

sub initialize_bootstrap_logging {
    my $self = shift;

    my $bootstrap_config = q|
        log4perl.rootLogger                               = WARN, Screen

        log4perl.appender.Screen                          = Log::Log4perl::Appender::Screen
        log4perl.appender.Screen.layout                   = Log::Log4perl::Layout::PatternLayout
        log4perl.appender.Screen.layout.ConversionPattern = %p [%c] %m%n
        #log4perl.appender.Screen.Threshold                = WARN

    |;
    Log::Log4perl->init(\$bootstrap_config);

    Log::Any::Adapter->set('Log4perl');
    $self->log->debug('Initialized bootstrap logging.');
    $self->log->debug('Hooked Log::Log4perl logging into Log::Any adapter.');

} # initialize_bootstrap_logging


=head2 initialize_logging

Initializes the entire logging system.

This includes calls to:

=over

=item * C<initialize_basic_logging>

=item * C<set_log_level>

=item * C<adjust_verbosity>

=back

Sets the C<is_logging_initialized> attribute to true once done.

=cut

sub initialize_logging {
    my $self = shift;

    if ( $self->is_logging_initialized ) {
        $self->log->warn('Logging already initialized.  Ignoring.');
    }
    else {
        $self->is_logging_initialized(1);

        $self->initialize_basic_logging;
    }

    my $q = $self->quietness   // 0;
    my $v = $self->verbosity // 0;
    $self->log->debug(sprintf 'self.quietness=%d', $q);
    $self->log->debug(sprintf 'self.verbosity=%d', $v);

    if ( defined $v || defined $q ) {
        $self->adjust_verbosity($v - $q);
    }

} # initialize_logging


=method log_level_to_priority

Returns a raw log level given by the specified string representation of
the log level.

Parameters:

=over

=item * C<level> String

A string representation of a log level.
Case is ignored.
One of the following:

=over

=item * C<OFF>

=item * C<FATAL>

=item * C<ERROR>

=item * C<INFO>

=item * C<DEBUG>

=item * C<TRACE>

=item * C<ALL>

=back

=back


Returns:

=over

=item * C<raw log_level>

=back

=cut

sub log_level_to_priority {
    my $self  = shift;
    my $level = shift;

    my $l_output = $level;

    $level = uc $level;
    my $is_valid = Log::Log4perl::Level::is_valid($level);

    unless ( $is_valid ) {
        # NB: This is outputing the message durint easy_init?
        $self->log->warn(
                sprintf 'Log level "%s" is not valid.  Ignoring.', $l_output);
        return;
    }

    return Log::Log4perl::Level::to_priority($level);
} # log_level_to_priority


=method root_logger

Returns the root logger.

Returns:

=over

=item * C<logger object>

=back

=cut

sub root_logger {
    return Log::Log4perl::get_logger('');
} # root_logger


=method set_log_level

Sets the level of the root logger.

Parameters:

=over

=item * C<level> String

A string representation of a log level.
Case is ignored.
One of the following:

=over

=item * C<OFF>

=item * C<FATAL>

=item * C<ERROR>

=item * C<INFO>

=item * C<DEBUG>

=item * C<TRACE>

=item * C<ALL>

=back

=back

=cut

sub set_log_level {
    my $self  = shift;
    my $level = shift;

    my $l_output = $level;

    my $priority  = $self->log_level_to_priority($level);
    unless ( $priority ) {
        return;
    }

    my $root = $self->root_logger;
    my $old_level = $root->level;

    $self->log->info(sprintf 'Setting log-level to "%s".', $l_output);
    $root->level($priority);

} # set_log_level


1;
