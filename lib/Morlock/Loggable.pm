package Morlock::Loggable;


# ABSTRACT: A role which provides a logger

# COPYRIGHT


use Moose::Role;

use Log::Any;


has log => (
    is      => 'ro',
    builder => '_get_logger',
    lazy    => 1,
);


sub _get_logger {
    my $self = shift;

    my $cat = ref $self;

    return Log::Any->get_logger(category => $cat);
} # _get_logger {


1;
